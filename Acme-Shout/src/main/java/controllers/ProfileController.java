/*
 * ProfileController.java
 * 
 * Copyright (C) 2018 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import domain.Calculator;

@Controller
@RequestMapping("/profile")
public class ProfileController extends AbstractController {

	// Action-1 ---------------------------------------------------------------		

	@RequestMapping("/quotes")
	public ModelAndView action1() {
		ModelAndView result;
		List<String> quotes = new ArrayList<String>();
		quotes.add("Todos somos aficionados.La vida es tan corta que no da para mas -- Charles Chaplin");
		quotes.add("La vida es una obra teatral que no importa cuanto haya durado, sino lo bien que haya sido representada -- Seneca");
		quotes.add("No hay paon mas ilusa y fanatica que el odio. -- George Gordon");
		quotes.add("Solo le falta el tiempo a quien no sabe aprovecharlo. -- Jovellanos");
		quotes.add("La alegria cuanto mas se gasta, mas queda. -- Ralph Waldo Emerson");

		Collections.shuffle(quotes);
		quotes = quotes.subList(0, 3);

		result = new ModelAndView("profile/quotes");
		result.addObject("quotes", quotes);

		return result;
	}
	// Action-2 ---------------------------------------------------------------		

	@RequestMapping(value = "/calculator", method = RequestMethod.GET)
	public ModelAndView action2Get() {
		ModelAndView result;
		Calculator calculator;

		calculator = new Calculator();

		result = new ModelAndView("profile/calculator");
		result.addObject("calculator", calculator);

		return result;
	}

	@RequestMapping(value = "/calculator", method = RequestMethod.POST)
	public ModelAndView action2Post(@Valid final Calculator calculator, final BindingResult binding) {
		ModelAndView result;

		calculator.compute();

		result = new ModelAndView("profile/calculator");
		result.addObject("calculator", calculator);

		return result;
	}
	// Action-3 ---------------------------------------------------------------		

	@RequestMapping("/panic")
	public ModelAndView action3() {
		throw new RuntimeException("Oops! An *expected* exception was thrown. This is normal behaviour.");
	}

}
